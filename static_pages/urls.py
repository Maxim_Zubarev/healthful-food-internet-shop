from django.contrib import admin
from django.urls import path
from .views import *

app_name = "static_pages"

urlpatterns = [
    path('contacts/', contacts_view, name='contacts_view'),
    path('add_question/', add_question_view, name='add_question_view'),
    path('about/', about_page, name='about_page'),
    path('delivery/', delivery_page, name='delivery_page'),
    path('policy/', policy_page, name='policy_page'),
]
