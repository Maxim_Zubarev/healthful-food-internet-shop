from django.contrib import admin
from .models import *


class ContactAdmin(admin.ModelAdmin):
    list_display = ['phone', 'address', 'email', 'created', 'updated']


class QuestionAdmin(admin.ModelAdmin):
    list_display = ['name', 'email', 'created']


class StandartAdmin(admin.ModelAdmin):
    list_display = ['created', 'updated']


admin.site.register(Contacts, ContactAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(About, StandartAdmin)
admin.site.register(Delivery, StandartAdmin)
admin.site.register(Policy, StandartAdmin)
