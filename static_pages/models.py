from django.db import models
from tinymce.models import HTMLField


class Contacts(models.Model):
    email = models.EmailField(verbose_name='Email')
    phone = models.CharField(max_length=20, verbose_name='Номер телефона')
    address = models.CharField(max_length=256, verbose_name='Адрес')
    content = HTMLField(verbose_name='Краткое описание')
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)
    title = models.CharField(max_length=255, null=True, blank=True, default=None)
    keywords = models.CharField(max_length=255, null=True, blank=True, default=None)
    description_seo = models.TextField(null=True, blank=True, default=None)

    class Meta:
        verbose_name = 'Контакты'
        verbose_name_plural = 'Контакты'


class Question(models.Model):
    name = models.CharField(max_length=100, verbose_name='Имя')
    email = models.EmailField(verbose_name='Email')
    question = models.TextField(verbose_name='Вопрос')
    created = models.DateTimeField(auto_now_add=True, auto_now=False)

    class Meta:
        verbose_name = 'Вопрос'
        verbose_name_plural = 'Вопросы'


class About(models.Model):
    content = HTMLField(verbose_name='Текст страницы')
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)
    title = models.CharField(max_length=255, null=True, blank=True, default=None)
    keywords = models.CharField(max_length=255, null=True, blank=True, default=None)
    description_seo = models.TextField(null=True, blank=True, default=None)

    class Meta:
        verbose_name = 'Страница о нас'
        verbose_name_plural = 'Страница о нас'


class Delivery(models.Model):
    content = HTMLField(verbose_name='Текст страницы')
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)
    title = models.CharField(max_length=255, null=True, blank=True, default=None)
    keywords = models.CharField(max_length=255, null=True, blank=True, default=None)
    description_seo = models.TextField(null=True, blank=True, default=None)

    class Meta:
        verbose_name = 'Страница доставка и оплата'
        verbose_name_plural = 'Страница доставка и оплата'


class Policy(models.Model):
    content = HTMLField(verbose_name='Текст страницы')
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    class Meta:
        verbose_name = 'Политика конфеденциальности'
        verbose_name_plural = 'Политика конфеденциальности'
