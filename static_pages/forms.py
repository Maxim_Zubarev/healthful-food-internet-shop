from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django import forms
from .models import Question


class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        exclude = ['created']

    def __init__(self, *args, **kwargs):
        super(QuestionForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'

    # def __init__(self, *args, **kwargs):
    #     super().__init__(*args, **kwargs)
    #     self.helper = FormHelper()
    #     self.helper.form_method = 'post'
    #     self.helper.wrapper_class = 'row'
    #     self.helper.label_class = 'col-lg-4'
    #     self.helper.field_class = 'col-lg-8'
    #     self.helper.add_input(Submit('submit', 'Отправить', css_class='add-to-cart2'))
