from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect
from products.models import Category
from static_pages.forms import QuestionForm
from static_pages.models import Contacts, About, Delivery, Policy


def contacts_view(request):
    contacts = Contacts.objects.latest('id')
    categories = Category.objects.filter(is_active=True)
    form = QuestionForm()
    context = {
        'contacts': contacts,
        'categories': categories,
        'form': form,
    }
    return render(request, 'static_pages/contacts.html', context)


def add_question_view(request):
    contacts = Contacts.objects.latest('id')
    categories = Category.objects.filter(is_active=True)
    form = QuestionForm(request.POST)
    context = {
        'contacts': contacts,
        'categories': categories,
    }
    if request.method == 'POST':
        if form.is_valid():
            form.save(commit=True)
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

    else:
        context['form'] = form
        return render(request, 'static_pages/contacts.html', context)


def about_page(request):
    content = About.objects.latest('id')
    categories = Category.objects.filter(is_active=True)
    return render(request, 'static_pages/about.html', {'content': content, 'categories': categories, })


def delivery_page(request):
    content = Delivery.objects.latest('id')
    categories = Category.objects.filter(is_active=True)
    return render(request, 'static_pages/delivery.html', {'content': content, 'categories': categories, })


def policy_page(request):
    content = Policy.objects.latest('id')
    categories = Category.objects.filter(is_active=True)
    return render(request, 'static_pages/policy.html', {'content': content, 'categories': categories, })
