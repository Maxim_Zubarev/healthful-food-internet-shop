from crispy_forms.helper import FormHelper
from django import forms
from django.contrib.auth.models import User
from account.models import Profile


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email')

    # def clean(self):
    #     email = self.cleaned_data['email']
    #     if User.objects.exclude(id=User.id).filter(email=email).exists():
    #         raise forms.ValidationError('Пользователь с данным e-mail уже зарегистрирован!')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        self.fields['email'].required = True
        self.helper.wrapper_class = 'row'
        self.helper.label_class = 'col-lg-4'
        self.helper.field_class = 'col-lg-8'


class UserFormReg(UserForm):
    password_check = forms.CharField(widget=forms.PasswordInput, label='Повторите пароль')
    password = forms.CharField(widget=forms.PasswordInput, label='Пароль')

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password', 'password_check')

    def clean(self):
        username = self.cleaned_data['username']
        email = self.cleaned_data['email']
        password = self.cleaned_data['password']
        password_check = self.cleaned_data['password_check']
        if User.objects.filter(username=username).exists():
            raise forms.ValidationError('Пользователь с данным логином уже зарегистрирован!')
        if User.objects.filter(email=email).exists():
            raise forms.ValidationError('Пользователь с данным e-mail уже зарегистрирован!')
        if password != password_check:
            raise forms.ValidationError('Ваши пароли не совпадают! Попробуйте снова!')


class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('phone_number', 'address')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.wrapper_class = 'row'
        self.helper.label_class = 'col-lg-4'
        self.helper.field_class = 'col-lg-8'


class LoginForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(widget=forms.PasswordInput)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.fields['username'].label = 'Логин'
        self.fields['password'].label = 'Пароль'

