from django.urls import path
from .views import *

app_name = "account"

urlpatterns = [
    path('account/data', account_data, name='account_data'),
    path('account/orders', account_orders, name='account_orders'),
    path('account/active_orders', account_orders_active, name='account_orders_active'),
    path('registration/', registration_view, name='registration_view'),
    path('update/', update_profile, name='update_profile'),
    path('logout/', logout_view, name='logout_view'),
    path('login/', login_view, name='login_view'),
    path('order/<int:order_id>', order_desc_view, name='order_desc_view'),
]
