from django.contrib import auth
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from cart.cart import Cart
from cart.models import CartItems, Cart as ModelCart
from products.models import Product
from .forms import UserForm, ProfileForm, UserFormReg
from orders.models import Order, OrderItem


@login_required
def account_data(request):
    user_form = UserForm(instance=request.user)
    profile_form = ProfileForm(instance=request.user.profile)
    context = {
        'user_form': user_form,
        'profile_form': profile_form,
    }
    return render(request, 'account/account.html', context)


@login_required
def account_orders(request):
    orders = Order.objects.filter(user=request.user, email=request.user.email)
    return render(request, 'account/orders.html', {'orders': orders, })


@login_required
def account_orders_active(request):
    orders = Order.objects.filter(user=request.user, email=request.user.email, status__id__in=[1, 2, 3])
    return render(request, 'account/orders.html', {'orders': orders, })


@login_required
def order_desc_view(request, order_id):
    order = get_object_or_404(Order, id=order_id)
    order_items = OrderItem.objects.select_related('product').filter(order_id=order_id)
    context = {
        'order': order,
        'order_items': order_items
    }
    return render(request, 'account/order_desc.html', context)


def registration_view(request):
    if request.method == 'POST':
        user_form = UserFormReg(request.POST)
        profile_form = ProfileForm(request.POST)
        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save()
            user.set_password(user_form.cleaned_data.get('password'))
            user.save()
            user.profile.phone_number = profile_form.cleaned_data.get('phone_number')
            user.profile.address = profile_form.cleaned_data.get('address')
            user.profile.save()
            ModelCart.objects.create(user=user)
            login(request, user)
            return HttpResponseRedirect(reverse('products:main_page'))
    else:
        user_form = UserFormReg()
        profile_form = ProfileForm()
    context = {
        'user_form': user_form,
        'profile_form': profile_form,
    }
    return render(request, 'account/registration.html', context)


@login_required
def logout_view(request):
    auth.logout(request)
    return HttpResponseRedirect(reverse('products:main_page'))


def login_view(request):
    data = {}
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                data['login'] = True
                login(request, user)
                cart = Cart(request)
                items = CartItems.objects.filter(cart__user_id=user.id)
                if cart.cart:
                    for key in cart.cart:
                        product = get_object_or_404(Product, id=key)
                        cart.add(product=product, quantity=cart.cart[key]['quantity'] - 2)
                for item in items:
                    cart.add(product=item.product, quantity=item.quantity, update_bd=False)

            else:
                data['login'] = False
                data['error'] = 'nonactive'
        else:
            data['success'] = False
        return JsonResponse(data)


@login_required
@transaction.atomic
def update_profile(request):
    if request.method == 'POST':
        user_form = UserForm(request.POST, instance=request.user)
        profile_form = ProfileForm(request.POST, instance=request.user.profile)
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            return HttpResponseRedirect(reverse('account:account_data'))
    else:
        user_form = UserForm(instance=request.user)
        profile_form = ProfileForm(instance=request.user.profile)
    return render(request, 'account/account.html', {
        'user_form': user_form,
        'profile_form': profile_form,
    })
