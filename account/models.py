from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


class Profile(models.Model):
    user = models.OneToOneField(User, verbose_name='Логин пользователя', on_delete=models.CASCADE)
    phone_number = models.CharField(verbose_name='Номер телефона', max_length=20, blank=True)
    address = models.CharField(verbose_name='Адрес', max_length=255, blank=True)
    created = models.DateTimeField(verbose_name='Дата создания', auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(verbose_name='Дата обновления', auto_now_add=False, auto_now=True)

    def __str__(self):
        return self.address

    class Meta:
        verbose_name = 'Профиль'
        verbose_name_plural = 'Профили'


@receiver(post_save, sender=User)
def create_or_update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    User.profile = property(lambda u: Profile.objects.get_or_create(user=u)[0])
