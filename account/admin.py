from django.contrib import admin

# from account.models import Customer


# class CustomerAdmin(admin.ModelAdmin):
#     list_display = ['id', 'username', 'first_name', 'last_name', 'email', 'created']
#     list_filter = ['created', 'is_active']
#
#
# admin.site.register(Customer, CustomerAdmin)
from account.models import Profile


class ProfileAdmin(admin.ModelAdmin):
    list_display = ['id', 'user', 'phone_number', 'address', 'created', 'updated']
    list_filter = ['created', 'updated']


admin.site.register(Profile, ProfileAdmin)
