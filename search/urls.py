from django.contrib import admin
from django.urls import path, include
from .views import *


app_name = "search"


urlpatterns = [
    path('search', SearchView.as_view(), name='search_view'),
]
