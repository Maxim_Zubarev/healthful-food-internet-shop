from django.db.models import Q
from django.shortcuts import render
from django.views import View

from products.models import Product


class SearchView(View):
    def get(self, request, *args, **kwargs):
        query = self.request.GET.get('q')
        founded_products = Product.objects.filter(
            Q(name__icontains=query) |
            Q(description__icontains=query)
        )
        context = {
            'founded_products': founded_products,
        }
        return render(request, 'search/search.html', context)
