from products.models import Category


def get_categories(request):
    categories = Category.objects.filter(is_active=True)
    return {
        'categories': categories,
    }