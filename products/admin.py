from django.contrib import admin
from .models import *


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug', 'is_active']
    list_editable = ['is_active']
    prepopulated_fields = {'slug': ('name', )}


class ProductAdmin(admin.ModelAdmin):
    list_display = ['name', 'category', 'price', 'is_new', 'is_active', 'created', 'updated']
    readonly_fields = ('rating', 'review_amount')
    exclude = ['discount_size']
    list_filter = ['category', 'is_active', 'created', 'updated']
    list_editable = ['is_new', 'is_active']
    prepopulated_fields = {'slug': ('name', )}


class ReviewAdmin(admin.ModelAdmin):
    list_display = ['name', 'email', 'product', 'rating', 'created']
    list_filter = ['product']


admin.site.register(Category, CategoryAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Brand)
admin.site.register(Review, ReviewAdmin)
