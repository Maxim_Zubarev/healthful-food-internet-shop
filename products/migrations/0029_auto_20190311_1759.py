# Generated by Django 2.1.3 on 2019-03-11 14:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0028_auto_20190311_1757'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='review_amount',
            field=models.PositiveIntegerField(blank=True, default=0, null=True, verbose_name='Количетсво отзывов'),
        ),
    ]
