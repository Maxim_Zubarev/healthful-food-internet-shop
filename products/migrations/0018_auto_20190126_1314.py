# Generated by Django 2.1.3 on 2019-01-26 10:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0017_auto_20190126_1313'),
    ]

    operations = [
        migrations.AlterField(
            model_name='review',
            name='short_desc',
            field=models.CharField(default='', max_length=255, verbose_name='Ваше мнение о товаре в одном предложение'),
        ),
    ]
