# Generated by Django 2.1.3 on 2019-01-21 13:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0011_auto_20190121_1657'),
    ]

    operations = [
        migrations.AlterField(
            model_name='review',
            name='body',
            field=models.TextField(default=''),
        ),
        migrations.AlterField(
            model_name='review',
            name='short_desc',
            field=models.TextField(default=''),
        ),
    ]
