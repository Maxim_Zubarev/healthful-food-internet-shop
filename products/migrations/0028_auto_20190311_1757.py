# Generated by Django 2.1.3 on 2019-03-11 14:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0027_product_review_amount'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='review_amount',
            field=models.PositiveIntegerField(blank=True, default=0, verbose_name='Количетсво отзывов'),
        ),
    ]
