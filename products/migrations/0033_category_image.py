# Generated by Django 2.1.3 on 2019-04-15 15:12

from django.db import migrations, models
import products.models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0032_auto_20190314_1336'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='image',
            field=models.ImageField(default='', upload_to=products.models.image_folder, verbose_name='Изображение'),
        ),
    ]
