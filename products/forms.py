from django import forms
from .models import Review


class AddReviewForm(forms.ModelForm):
    RATING_CHOICES = (
        (5, '1'),
        (4, '2'),
        (3, '3'),
        (2, '4'),
        (1, '5'),
    )

    class Meta:
        model = Review
        exclude = ('product', 'created')

    rating = forms.ChoiceField(choices=RATING_CHOICES, widget=forms.RadioSelect(attrs={
        'class': 'star-rating__input'
    }))
    short_desc = forms.CharField(widget=forms.TextInput(attrs={
        'placeholder': 'Пример: Очень доволен покупкой',
        'class': 'form-control'
    }))
    body = forms.CharField(widget=forms.Textarea(attrs={
        'placeholder': 'Чем вам понравился/не понравился товар?',
        'class': 'form-control'
    }))

    def __init__(self, *args, **kwargs):
        super(AddReviewForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs.update({'class': 'form-control'})
        self.fields['email'].widget.attrs.update({'class': 'form-control'})
