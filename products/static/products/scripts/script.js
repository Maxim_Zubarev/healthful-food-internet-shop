$(document).ready(function () {
   $('.dec').click(function () {
        var $input = $(this).parent().find('#id_quantity');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();
        return false;
    });
    $('.inc').click(function () {
        var $input = $(this).parent().find('#id_quantity');
        $input.val(parseInt($input.val()) + 1);
        $input.change();
        return false;
    });

    function windowSize(){
        if ($(window).width() <= '991') {
            $("#cart").attr("href", "/cart/").attr("data-toggle", "#");
            $(".basket-items").removeClass("dropdown-menu");
            $(".login-mobile").css("display", "block");
        } else {
            $("#cart").attr("href", "#").attr("data-toggle", "dropdown");
            $(".basket-items").addClass("dropdown-menu");
            $(".login-mobile").css("display", "none");
        }
    }
    $(window).on('load resize',windowSize);

    $("#myTab1").on("click","a", function (event) {
        var anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $(anchor.attr('href')).offset().top-50
        }, 777);
        event.preventDefault();
        if (anchor[0]['outerText'] == 'Добавить отзыв'){
            $('#add-review-tab').tab('show');
        } else {
            $('#review-tab').tab('show');
        }
        return false;
    });

    $('#add-review-btn').on('click', function (e) {
       e.preventDefault();
       $('#add-review-tab').tab('show');
    });

    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= 50) {
            $("#nav-fix-top").addClass("fixed-top bg").removeClass("bg-tr");
        } else {
            $("#nav-fix-top").removeClass("fixed-top bg").addClass("bg-tr");
        }
    });
});