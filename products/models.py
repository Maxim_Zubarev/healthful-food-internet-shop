from django.db import models
from django.db.models import Sum
from django.db.models.signals import post_save, post_delete, pre_delete
from django.dispatch import receiver
from django.shortcuts import get_object_or_404
from django.urls import reverse
import django_filters
from tinymce.models import HTMLField


class Category(models.Model):
    name = models.CharField(max_length=100)
    slug = models.SlugField(db_index=True, unique=True)
    is_active = models.BooleanField(default=True)
    title = models.CharField(max_length=255, null=True, blank=True, default=None)
    keywords = models.CharField(max_length=255, null=True, blank=True, default=None)
    description_seo = models.TextField(null=True, blank=True, default=None)
    text = HTMLField(verbose_name='Описание')

    def __str__(self):
        return '%s' % self.name

    def get_absolute_url(self):
        return reverse('products:category_detail', args=[self.slug])

    class Meta:
        ordering = ['name']
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'


class Brand(models.Model):
    name = models.CharField(max_length=255)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return '%s' % self.name

    class Meta:
        verbose_name = 'Бренд товара'
        verbose_name_plural = 'Бренды товаров'


def image_folder(instance, filename):
    filename = instance.slug + '.' + filename.split('.')[1]
    return "{0}/{1}".format(instance.slug, filename)


class Product(models.Model):
    name = models.CharField(verbose_name='Нвзвание', max_length=64, null=True, default=None)
    article = models.IntegerField(verbose_name='Артикул', default=0)
    slug = models.SlugField(default='', db_index=True)
    price = models.DecimalField(verbose_name='Цена', max_digits=10, decimal_places=2, default=0)
    discount = models.DecimalField(verbose_name='Цена со скидкой', default=0, max_digits=10, decimal_places=2)
    discount_size = models.IntegerField(verbose_name='Скидка', default=0)
    is_new = models.BooleanField(verbose_name='Новинка', default=True)
    category = models.ForeignKey(Category, verbose_name='Категория', null=True, default=None, on_delete=models.CASCADE)
    brand = models.ForeignKey(Brand, verbose_name='Бренд', null=True, default=None, on_delete=models.CASCADE)
    short_description = HTMLField(verbose_name='Краткое описание')
    description = HTMLField(verbose_name='Полное описание')
    image = models.ImageField(verbose_name='Изображение', upload_to=image_folder, default='')
    is_active = models.BooleanField(verbose_name='Показывать', default=True)
    rating = models.DecimalField(verbose_name='Рейтинг', max_digits=5, decimal_places=2, default=0)
    review_amount = models.IntegerField(verbose_name='Количество отзывов', default=0, blank=True, null=True)
    created = models.DateTimeField(verbose_name='Дата создания', auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(verbose_name='Дата обновления', auto_now_add=False, auto_now=True)
    title = models.CharField(max_length=255, null=True, blank=True, default=None)
    keywords = models.CharField(max_length=255, null=True, blank=True, default=None)
    description_seo = models.TextField(null=True, blank=True, default=None)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('products:product_detail', args=[self.id, self.slug])

    def get_discount(self):
        product = get_object_or_404(Product, id=self.id, is_active=True)
        if product.discount > 0:
            discount_size = round((product.price - product.discount) / (product.price / 100), 0)
        else:
            discount_size = 0
        return discount_size

    def get_price(self):
        return self.discount if self.discount != 0 else self.price

    class Meta:
        ordering = ['name']
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'


class Review(models.Model):
    product = models.ForeignKey(Product, verbose_name='Продукт', default=None, on_delete=models.CASCADE)
    name = models.CharField(verbose_name='Имя', max_length=255, default=None, null=True)
    email = models.EmailField(default=None)
    rating = models.IntegerField(verbose_name='Рейтинг', default='5')
    short_desc = models.CharField(verbose_name='Мнение о товаре в одном предложение', max_length=255, default='')
    body = models.TextField(verbose_name='Обзор', default='')
    verificated = models.BooleanField(default=False, verbose_name='Показывать на сайте')
    created = models.DateTimeField(auto_now_add=True, auto_now=False)

    class Meta:
        verbose_name_plural = 'Отзывы'
        verbose_name = 'Отзыв'

    def __unicode__(self):
        return u'%s' % self.name


@receiver(post_save, sender=Review)
def set_review(sender, instance, **kwargs):
    reviews_sum = Review.objects.filter(product__name=instance.product.name, verificated=True).aggregate(Sum('rating'))
    if reviews_sum['rating__sum'] is not None:
        set_rating(instance)


@receiver(post_delete, sender=Review)
def delete_review(sender, instance, **kwargs):
    reviews_sum = Review.objects.filter(product__name=instance.product.name, verificated=True).aggregate(Sum('rating'))
    if reviews_sum['rating__sum'] is not None:
        set_rating(instance)
    else:
        instance.product.rating = 0
        instance.product.review_amount = 0
        instance.product.save()


def set_rating(instance):
    reviews_sum = Review.objects.filter(product__name=instance.product.name, verificated=True).aggregate(Sum('rating'))
    reviews_count = Review.objects.filter(product__name=instance.product.name, verificated=True).count()
    reviews_rating = round(reviews_sum['rating__sum'] / reviews_count, 1)
    instance.product.rating = reviews_rating
    instance.product.review_amount = reviews_count
    instance.product.save()