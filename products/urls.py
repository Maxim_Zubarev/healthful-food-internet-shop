"""int_shop URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from .views import *

app_name = "products"

urlpatterns = [
    path('', main_page, name='main_page'),
    path('product/<int:id>/<slug:slug>', product_detail, name='product_detail'),
    path('category/<slug:slug>', category_detail, name='category_detail'),
    path('add_review/<int:id>', add_review, name='add_review'),
    path('selection/new', selection_new, name='selection_new'),
    path('selection/sale', selection_sale, name='selection_sale'),
]
