from django.core.paginator import Paginator
from django.db.models import Sum
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, render_to_response
from django.template import RequestContext
from django.utils import timezone

from cart.forms import CartAddProductForm
from products.filters import ProductFilter
from products.models import *
from cart.cart import Cart
from slider.models import Slider
from static_pages.models import Contacts
from .forms import AddReviewForm


def main_page(request):
    products_new = Product.objects.filter(is_active=True, is_new=True)[:8]
    products_sale = Product.objects.filter(is_active=True, discount__gt=0)[:8]
    contacts = Contacts.objects.latest('id')
    sliders = Slider.objects.filter(is_active=True)
    cart_product_form = CartAddProductForm()
    cart = Cart(request)
    context = {
        'products_new': products_new,
        'products_sale': products_sale,
        'cart_product_form': cart_product_form,
        'cart': cart,
        'contacts': contacts,
        'sliders': sliders
    }
    return render(request, 'products/main_part_on_main_page.html', context)


def product_detail(request, id, slug):
    product = get_object_or_404(Product, id=id, slug=slug, is_active=True)
    products = Product.objects.filter(is_active=True, category=product.category).exclude(id=product.id)
    reviews = Review.objects.filter(product__name=product.name, verificated=True)
    cart_product_form = CartAddProductForm()
    review_form = AddReviewForm()
    cart = Cart(request)
    contacts = Contacts.objects.latest('id')
    context = {
        'products': products,
        'product': product,
        'reviews': reviews,
        'cart_product_form': cart_product_form,
        'review_form': review_form,
        'cart': cart,
        'contacts': contacts,
    }
    return render(request, 'products/product_detail.html', context)


def category_detail(request, slug):
    category = Category.objects.get(slug=slug)

    if request.GET.get('sort') is None:
        sort = 'id'
    else:
        sort = request.GET.get('sort')

    if request.GET.get('type') is None or request.GET.get('type') == 'all':
        products_list = Product.objects.filter(is_active=True, category=category).order_by(sort)
    elif request.GET.get('type') == 'new':
        products_list = Product.objects.filter(is_active=True, category=category, is_new=True).order_by(sort)
    elif request.GET.get('type') == 'sale':
        products_list = Product.objects.filter(is_active=True, category=category, discount__gt=0).order_by(sort)

    paginator = Paginator(products_list, 16)
    page = request.GET.get('page')
    products = paginator.get_page(page)
    cart_product_form = CartAddProductForm()
    cart = Cart(request)
    contacts = Contacts.objects.latest('id')
    context = {
        'products': products,
        'category': category,
        'cart_product_form': cart_product_form,
        'cart': cart,
        'contacts': contacts,
        # 'filter': filter,
    }
    return render(request, 'products/category_detail.html', context)


def add_review(request, id):
    form = AddReviewForm(request.POST)
    product = get_object_or_404(Product, id=id)
    if form.is_valid():
        review = form.save(commit=False)
        review.product = product
        review.created = timezone.now()
        review.save()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def selection_new(request):
    products_list = Product.objects.filter(is_active=True, is_new=True)
    paginator = Paginator(products_list, 16)
    page = request.GET.get('page')
    products = paginator.get_page(page)
    title = 'Новинки'
    context = {
        'products': products,
        'title': title
    }
    return render(request, 'products/selection.html', context)


def selection_sale(request):
    products_list = Product.objects.filter(is_active=True, discount__gt=0)
    paginator = Paginator(products_list, 16)
    page = request.GET.get('page')
    products = paginator.get_page(page)
    title = 'Скидки'
    context = {
        'products': products,
        'title': title
    }
    return render(request, 'products/selection.html', context)
