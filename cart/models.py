from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save, pre_save, post_delete
from django.dispatch import receiver

from products.models import Product


class Cart(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Пользователь')
    total = models.DecimalField(default=0.00, max_digits=10, decimal_places=2, verbose_name='Всего')

    def __str__(self):
        return self.user.username + 'cart'

    def get_total(self):
        return sum(item.get_cost() for item in self.items.all())

    class Meta:
        verbose_name_plural = 'Корзины'
        verbose_name = 'Корзина'


class CartItems(models.Model):
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE, related_name='items', verbose_name='Корзина №')
    product = models.ForeignKey(Product, on_delete=models.CASCADE, verbose_name='Продукт')
    quantity = models.PositiveIntegerField(default=1, verbose_name='Количество')
    price = models.DecimalField(default=0.00, max_digits=10, decimal_places=2, verbose_name='Сумма')

    def __str__(self):
        return '{}'.format(self.cart.id)

    def get_cost(self):
        return self.product.get_price() * self.quantity


@receiver(pre_save, sender=CartItems)
def update_cart(sender, instance, **kwargs):
    instance.price = instance.get_cost()


@receiver(post_save, sender=CartItems)
@receiver(post_delete, sender=CartItems)
def update_total(sender, instance, **kwargs):
    instance.cart.total = instance.cart.get_total()
    instance.cart.save()
