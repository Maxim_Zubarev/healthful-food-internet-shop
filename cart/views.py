from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.http import require_POST
from products.models import Product
from .cart import Cart
from .forms import CartAddProductForm


@require_POST
def cart_add(request, product_id):
    cart = Cart(request)
    product = get_object_or_404(Product, id=product_id)
    form = CartAddProductForm(request.POST)
    data = {}
    if form.is_valid():
        cd = form.cleaned_data
        cart.add(product=product, quantity=cd['quantity'], update_quantity=cd['update'])
        data['success'] = True
    data['amount'] = cart.__len__()
    return JsonResponse(data)


def cart_remove(request, product_id):
    cart = Cart(request)
    data = {}
    product = get_object_or_404(Product, id=product_id)
    if cart.remove(product):
        data['success'] = True
        data['amount'] = cart.__len__()
    return JsonResponse(data)


def cart_detail(request):
    cart = Cart(request)
    for item in cart:
        item['update_quantity_form'] = CartAddProductForm(
            initial={
                'quantity': item['quantity'],
                'update': True
            })
    context = {
        'cart': cart,
    }
    return render(request, 'cart/detail.html', context)
