from decimal import Decimal
from django.conf import settings
from django.shortcuts import get_object_or_404
from cart.models import Cart as productCart
from cart.models import CartItems
from products.models import Product


class Cart(object):
    def __init__(self, request):
        self.session = request.session
        cart = self.session.get(settings.CART_SESSION_ID)
        if request.user.is_authenticated:
            # self.user = request.user
            self.user = None
        else:
            self.user = None
        if not cart:
            cart = self.session[settings.CART_SESSION_ID] = {}
        self.cart = cart
        # self.sync(request)

    def add(self, product, quantity=1, update_quantity=False, update_bd=True):
        product_id = str(product.id)
        price = product.get_price()
        if product_id not in self.cart:
            self.cart[product_id] = {'quantity': 0,
                                     'price': str(price)}
        if update_quantity:
            self.cart[product_id]['quantity'] = quantity
        else:
            self.cart[product_id]['quantity'] += quantity
        self.save()
        if update_bd:
            if self.user is not None:
                items = CartItems.objects.filter(cart__user_id=self.user.id)
                update = False
                user_cart = get_object_or_404(productCart, user=self.user)
                for item in items:
                    if product == item.product:
                        update = True

                if update:
                    product_item = CartItems.objects.get(product=product)
                    product_item.quantity = quantity
                    product_item.save()
                else:
                    product_item = CartItems(cart_id=user_cart.id, product=product, quantity=quantity)
                    product_item.save()

    def save(self):
        self.session[settings.CART_SESSION_ID] = self.cart
        self.session.modified = True

    def remove(self, product):
        product_id = str(product.id)
        result = False
        del_from_session = False
        del_from_db = False
        if product_id in self.cart:
            del self.cart[product_id]
            self.save()
            del_from_session = True
        if self.user is not None:
            product = get_object_or_404(CartItems, cart__user_id=self.user.id, product=product)
            result = product.delete()
            if result[0] > 0:
                del_from_db = True
        # if del_from_db and del_from_session:
        #     result = True
        if del_from_session:
            result = True
        return result

    def __iter__(self):
        product_ids = self.cart.keys()
        products = Product.objects.filter(id__in=product_ids)
        for product in products:
            self.cart[str(product.id)]['product'] = product

        for item in self.cart.values():
            item['price'] = Decimal(item['price'])
            item['total_price'] = item['price'] * item['quantity']
            yield item

    def __len__(self):
        return sum(item['quantity'] for item in self.cart.values())

    def get_total_price(self):
        return sum(Decimal(item['price']) * item['quantity'] for item in self.cart.values())

    def clear(self):
        del self.session[settings.CART_SESSION_ID]
        self.session.modified = True
        if self.user is not None:
            CartItems.objects.filter(cart__user_id=self.user.id).delete()

    def sync(self, request):
        if request.user.is_authenticated:
            item_keys = []
            items = CartItems.objects.filter(cart__user_id=self.user.id)
            for item in items:
                item_keys.append(str(item.product_id))
                if str(item.product_id) in self.cart.keys():
                    # print(self.cart.get(str(item.product_id))['quantity'])
                    if item.quantity != self.cart.get(str(item.product_id))['quantity']:
                        self.cart[str(item.product_id)]['quantity'] = item.quantity
                else:
                    self.cart[str(item.product_id)] = {'quantity': item.quantity,
                                                       'price': str(item.product.get_price())}
                    # cart = Cart(request)
                    # self.add(item.product, item.quantity, False, False)
            # self.save()
            for id in list(self.cart.keys()):
                if id not in item_keys:
                    self.cart.pop(id)

