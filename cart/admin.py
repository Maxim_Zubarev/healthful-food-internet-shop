from django.contrib import admin

from cart.models import Cart, CartItems


class CartItemInline(admin.TabularInline):
    model = CartItems
    readonly_fields = ('price', )
    raw_id_field = ['product']


class CartAdmin(admin.ModelAdmin):
    list_display = ['id', 'user']
    readonly_fields = ('total',)
    inlines = [CartItemInline]


admin.site.register(Cart, CartAdmin)
