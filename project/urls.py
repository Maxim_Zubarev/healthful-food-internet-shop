"""int_shop URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from django.utils.functional import curry
from django.views.defaults import permission_denied, page_not_found, server_error

handler403 = curry(permission_denied, exception=Exception('Permission Denied'), template_name='products/errors/403.html')
handler404 = curry(page_not_found, exception=Exception('Page not Found'), template_name='products/errors/404.html')
handler500 = curry(server_error, template_name='products/errors/500.html')


urlpatterns = [
    path('grappelli/', include('grappelli.urls')),
    path('admin/', admin.site.urls),
    path('cart/', include('cart.urls', namespace='cart')),
    path('order/', include('orders.urls', namespace='orders')),
    path('search/', include('search.urls', namespace='search')),
    path('', include('products.urls', namespace='products')),
    path('', include('account.urls', namespace='account')),
    path('', include('static_pages.urls', namespace='static_pages')),
    path('api/', include('api.urls', namespace='api')),
    path('auth/', include('djoser.urls')),
    path('auth/', include('djoser.urls.authtoken')),
    path('tinymce/', include('tinymce.urls')),
    path('fcm/', include('fcm.urls')),
    path('paypal/', include('paypal.standard.ipn.urls')),
    path('payment/', include('payment.urls', namespace='payment')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += [
        path('403', handler403),
        path('404', handler404),
        path('500', handler500),
    ]

