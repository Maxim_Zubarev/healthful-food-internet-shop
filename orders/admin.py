from django.contrib import admin
from django.http import HttpResponse

from .models import Order, OrderItem, Status
import csv
import datetime


class OrderItemInline(admin.TabularInline):
    model = OrderItem
    readonly_fields = ('price', 'total_price')
    raw_id_field = ['product']


def export_to_csv(modeladmin, request, queryset):
    opts = modeladmin.model._meta
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename={}.csv'.format(opts.verbose_name)
    writer = csv.writer(response)
    fields = [field for field in opts.get_fields() if not field.many_to_many and not field.one_to_many]
    # Write a first row with header information
    writer.writerow([field.verbose_name for field in fields])
    # Write data rows
    for obj in queryset:
        data_row = []
        for field in fields:
            value = getattr(obj, field.name)
            if isinstance(value, datetime.datetime):
                value = value.strftime('%d/%m/%Y')
            data_row.append(value)
        writer.writerow(data_row)
    return response


export_to_csv.short_description = 'Экспортировать в CSV'


class OrderAdmin(admin.ModelAdmin):
    list_display = ['id', 'user', 'first_name', 'phone_number', 'email', 'total_price',
                    'pay_method', 'status', 'paid', 'created']
    list_filter = ['paid', 'created', 'updated']
    inlines = [OrderItemInline]
    actions = [export_to_csv]


class StatusAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'created', 'updated']


admin.site.register(Order, OrderAdmin)
admin.site.register(Status, StatusAdmin)
