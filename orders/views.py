from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from django.urls import reverse

from .models import OrderItem, Status
from .forms import OrderCreateForm
from cart.cart import Cart
from .send_mail import send


def order_create(request):
    cart = Cart(request)
    if request.method == 'POST':
        form = OrderCreateForm(request.POST)
        if form.is_valid():
            order = form.save(commit=False)
            if request.user.is_authenticated:
                order.user = request.user

            order.status = Status.objects.all()[:1].get()
            order.save()
            for item in cart:
                OrderItem.objects.create(order=order, product=item['product'],
                                         price=item['product'].get_price(),
                                         quantity=item['quantity'])
            cart.clear()
            items = OrderItem.objects.filter(order=order.id)
            send(order, items)
            if order.pay_method != 'Наличными':
                request.session['order_id'] = order.id
                return redirect('payment:process')
            else:
                return render(request, 'orders/order/created.html', {'order': order, })

    if request.user.is_authenticated:
        data = {
            'first_name': request.user.first_name,
            'last_name': request.user.last_name,
            'email': request.user.email,
            'phone_number': request.user.profile.phone_number,
            'address': request.user.profile.address,
        }
        form = OrderCreateForm(initial=data)
    else:
        form = OrderCreateForm()
    return render(request, 'orders/order/create.html', {'cart': cart,
                                                        'form': form, })
