from django.conf import settings
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags


def send(order, items):
    msg = render_to_string('orders/order/email.html', {'order': order, 'items': items})
    plain_message = strip_tags(msg)
    title = 'Заказ № ' + str(order.id)
    send_mail(title, plain_message, settings.EMAIL_HOST_USER, [order.email])
