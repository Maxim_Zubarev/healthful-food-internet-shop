from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from fcm.utils import get_device_model
from pyfcm import FCMNotification

from products.models import Product
from project import settings


class Status(models.Model):
    name = models.CharField(verbose_name='Статус', max_length=24, blank=True, null=True, default=None)
    is_active = models.BooleanField(verbose_name='Активен', default=True)
    created = models.DateTimeField(verbose_name='Дата создания', auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(verbose_name='Дата обновления', auto_now_add=False, auto_now=True)

    def __str__(self):
        return "%s" % self.name

    class Meta:
        verbose_name = 'Статус заказа'
        verbose_name_plural = 'Статусы заказа'


class Order(models.Model):
    # PAY_CHOICES = (
    #     ('Кредитной картой', 'Кредитной картой'),
    #     ('Наличными', 'Наличными')
    # )

    user = models.ForeignKey(User, verbose_name='Пользователь', on_delete=models.CASCADE, default='',
                             blank=True, null=True)
    first_name = models.CharField(verbose_name='Имя', max_length=50)
    last_name = models.CharField(verbose_name='Фамилия', max_length=50, blank=True, default='')
    phone_number = models.CharField(verbose_name='Телефон', max_length=20)
    email = models.EmailField(verbose_name='Email')
    address = models.CharField(verbose_name='Адрес', max_length=250, blank=True)
    comment = models.TextField(verbose_name='Комментарий', default='', blank=True)
    # pay_method = models.CharField(verbose_name='Способ оплаты', max_length=20, default='', choices=PAY_CHOICES)
    pay_method = models.CharField(verbose_name='Способ оплаты', max_length=20, default='Наличными', blank=True)
    status = models.ForeignKey(Status, verbose_name='Статус заказа', on_delete=models.CASCADE, default='')
    created = models.DateTimeField(verbose_name='Дата создания', auto_now_add=True)
    updated = models.DateTimeField(verbose_name='Дата обновления', auto_now=True)
    paid = models.BooleanField(verbose_name='Оплачен', default=False)
    total_price = models.DecimalField(verbose_name='Сумма', max_digits=10, decimal_places=2, blank=True, null=True)
    # __original_mode = 'Новый'

    class Meta:
        ordering = ('-created', )
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'

    # def __init__(self, *args, **kwargs):
    #     super(Order, self).__init__(*args, **kwargs)
    #     self.__original_mode = self.status

    def __str__(self):
        return 'Заказ: {}'.format(self.id)

    def get_total_cost(self):
        return sum(item.get_cost() for item in self.items.all())

    # def save(self, force_insert=False, force_update=False, *args, **kwargs):
    #     if self.status != self.__original_mode:
    #         change_status(self.status, self.id, self.user_id)
    #
    #     super(Order, self).save(force_insert, force_update, *args, **kwargs)
    #     self.__original_mode = self.status


class OrderItem(models.Model):
    order = models.ForeignKey(Order, related_name='items', on_delete=models.CASCADE)
    product = models.ForeignKey(Product, related_name='order_items', on_delete=models.CASCADE)
    price = models.DecimalField(verbose_name='Цена', max_digits=10, decimal_places=2, blank=True, null=True)
    quantity = models.PositiveIntegerField(verbose_name='Количество', default=1)
    total_price = models.DecimalField(verbose_name='Сумма', max_digits=10, decimal_places=2, blank=True, null=True)

    def __str__(self):
        return '{}'.format(self.id)

    def get_cost(self):
        return self.price * self.quantity


@receiver(pre_save, sender=OrderItem)
def set_price_to_item(sender, instance, **kwargs):
    instance.price = instance.product.get_price()
    instance.total_price = instance.get_cost()


@receiver(post_save, sender=OrderItem)
def set_total_price_to_order(sender, instance, **kwargs):
    instance.order.total_price = instance.order.get_total_cost()
    instance.order.save()


# def change_status(new_status, order_id, user_id):
@receiver(pre_save, sender=Order)
def change_status(sender, instance, **kwargs):
    try:
        order = Order.objects.get(id=instance.id)
    except Order.DoesNotExist:
        order = None
    if order is not None:
        if instance.status != order.status:
            server_key = settings.FCM_APIKEY
            message_title = "Заказ №" + str(instance.id)
            message_body = "Статус заказа изменен на: " + str(instance.status)
            device = get_device_model()
            try:
                my_phone = device.objects.get(dev_id=instance.user_id)
                reg_id = my_phone.reg_id
                FCMNotification(api_key=server_key).notify_single_device(registration_id=reg_id,
                                                                         message_title=message_title,
                                                                         message_body=message_body)
            except device.DoesNotExist:
                pass

    # FCMNotification(api_key=server_key).notify_single_device(registration_id=reg_id2, message_body=message_body, data_message=data_message)
