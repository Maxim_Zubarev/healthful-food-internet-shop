from django import forms
from .models import Order


# CHOICES = [('credit', 'Банковская карта'),
#            ('cash', 'Наличные')]


class OrderCreateForm(forms.ModelForm):
    PAY_CHOICES = (
        ('Кредитной картой', 'Кредитной картой'),
        ('Наличными', 'Наличными')
    )

    class Meta:
        model = Order
        exclude = ['user', 'status', 'created', 'updated', 'paid', 'total_price']

    pay_method = forms.ChoiceField(choices=PAY_CHOICES, widget=forms.RadioSelect())

    def __init__(self, *args, **kwargs):
        super(OrderCreateForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'
        self.fields['pay_method'].widget.attrs.update({'class': 'custom-control-input'})
