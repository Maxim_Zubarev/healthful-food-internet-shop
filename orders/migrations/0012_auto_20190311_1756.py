# Generated by Django 2.1.3 on 2019-03-11 14:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0011_auto_20190304_1141'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='pay_method',
            field=models.CharField(choices=[('Кредитной картой', 'Кредитной картой'), ('Наличными', 'Наличными')], default='', max_length=20, verbose_name='Способ оплаты'),
        ),
    ]
