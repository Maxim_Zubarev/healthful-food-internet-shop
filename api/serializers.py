from django.contrib.auth.models import User
from rest_framework import serializers

from account.models import Profile
from cart.models import Cart, CartItems
from orders.models import Order, OrderItem
from orders.send_mail import send
from products.models import Product, Review, Category


class ProductSerializer(serializers.ModelSerializer):
    category_name = serializers.CharField(source='category', read_only=True)
    brand_name = serializers.CharField(source='brand', read_only=True)

    class Meta:
        model = Product
        exclude = ('is_active', 'created', 'updated', 'brand', )


class CategoriesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'


class OrderItemSerializer(serializers.ModelSerializer):
    product_name = serializers.CharField(source='product', read_only=True)
    cost = serializers.DecimalField(source='get_cost', read_only=True, max_digits=10, decimal_places=2)
    image = serializers.CharField(source='product.image.url', read_only=True)

    class Meta:
        model = OrderItem
        exclude = ('order', )


class OrderSerializer(serializers.ModelSerializer):
    user_name = serializers.CharField(source='user', read_only=True)
    status_name = serializers.CharField(source='status', read_only=True)
    items = OrderItemSerializer(many=True)
    total = serializers.DecimalField(source='get_total_cost', read_only=True, max_digits=10, decimal_places=2)

    class Meta:
        model = Order
        exclude = ('status', 'user', )


class OrdersSerializer(serializers.ModelSerializer):
    user_name = serializers.CharField(source='user', read_only=True)
    status_name = serializers.CharField(source='status', read_only=True)
    total = serializers.IntegerField(source='get_total_cost', read_only=True)

    class Meta:
        model = Order
        exclude = ('status', 'user', )


class CreateOrderSerializer(serializers.ModelSerializer):
    items = OrderItemSerializer(many=True)

    class Meta:
        model = Order
        exclude = ('updated', )

    def create(self, validated_data):
        items = validated_data.pop('items')
        order = Order.objects.create(**validated_data)
        try:
            order.user_id = self.validated_data['user'].id
            order.save()
        except KeyError:
            pass

        for item in items:
            OrderItem.objects.create(
                order=order,
                product=item['product'],
                price=item['product'].get_price(),
                quantity=item['quantity']
            )
        send(order, items)
        return order


class ReviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Review
        exclude = ('product', )


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ('id', 'address', 'phone_number')


class ProfileSerializer(serializers.ModelSerializer):
    profile = UserSerializer()

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'email', 'profile')

    def update(self, instance, validated_data):
        # instance.first_name = validated_data['first_name']
        # instance.last_name = validated_data['last_name']
        # instance.email = validated_data['email']
        # profile = instance.profile
        # profile.address = validated_data['profile']['address']
        # profile.phone_number = validated_data['profile']['phone_number']
        # profile.save()
        # instance.save()
        # return instance
        user = User.objects.get(email=validated_data['email'])
        user.first_name = validated_data['first_name']
        user.last_name = validated_data['last_name']
        user.email = validated_data['email']
        profile = user.profile
        profile.address = validated_data['profile']['address']
        profile.phone_number = validated_data['profile']['phone_number']
        profile.save()
        return user


class ProfileCreateSerializer(serializers.ModelSerializer):
    profile = UserSerializer()
    password = serializers.CharField(required=True, style={'input_type': 'password', 'placeholder': 'Пароль'})

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password', 'profile')

    def create(self, validated_data):
        if User.objects.filter(username=validated_data['username']).exists():
            raise serializers.ValidationError('Пользователь с данным логином уже зарегистрирован!')
        elif User.objects.filter(email=validated_data['email']).exists():
            raise serializers.ValidationError('Пользователь с данным e-mail уже зарегистрирован!')
        else:
            new_user = User.objects.create_user(
                username=validated_data['username'],
                email=validated_data['email'],
                password=validated_data['password'],
                first_name=validated_data['first_name'],
                last_name=validated_data['last_name']
            )
            profile = validated_data.pop('profile')
            user_profile = Profile.objects.get(user_id=new_user.id)
            user_profile.phone_number = profile['phone_number']
            user_profile.address = profile['address']
            user_profile.save()
            return new_user


class CartItemSerializer(serializers.ModelSerializer):
    product_name = serializers.CharField(source='product', read_only=True)

    class Meta:
        model = CartItems
        fields = '__all__'


class CartSerializer(serializers.ModelSerializer):
    items = OrderItemSerializer(many=True)

    class Meta:
        model = Cart
        fields = '__all__'

    def create(self, validated_data):
        items = validated_data.pop('items')
        # try:
        #     cart = Cart.objects.get(user_id=validated_data['user'].id)
        # except Cart.DoesNotExist:
        #     cart = None
        # if cart is not None:
        #     for item in items:
        #         try:
        #             cart_item = CartItems.objects.get(cart_id=cart.id, product_id=item['product'].id)
        #         except CartItems.DoesNotExist:
        #             cart_item = None
        #         if cart_item is not None:
        #             cart_item.quantity = item['quantity']
        #             cart_item.save()
        #         else:
        #             CartItems.objects.create(
        #                 cart=cart,
        #                 product=item['product'],
        #                 quantity=item['quantity'],
        #                 price=0
        #             )
        #     return cart
        # else:
        #     new_cart = Cart.objects.create(**validated_data)
        #     for item in items:
        #         CartItems.objects.create(
        #             cart=new_cart,
        #             product=item['product'],
        #             quantity=item['quantity'],
        #             price=item['product'].get_price()
        #         )
        #     return new_cart
        try:
            Cart.objects.filter(user_id=validated_data['user'].id).delete()
        except Cart.DoesNotExist:
            pass
        new_cart = Cart.objects.create(**validated_data)
        for item in items:
            CartItems.objects.create(
                cart=new_cart,
                product=item['product'],
                quantity=item['quantity'],
                price=item['product'].get_price()
            )
        return new_cart


class CartItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = CartItems
        fields = '__all__'

    def update(self, instance, validated_data):
        instance.quantity = validated_data.get('quantity', instance.quantity)
        instance.save()
        return instance


class CartItemAddSerializer(serializers.ModelSerializer):
    class Meta:
        model = CartItems
        fields = '__all__'

    def create(self, validated_data):
        try:
            item = CartItems.objects.get(cart_id=validated_data['cart'], product_id=validated_data['product'])
        except CartItems.DoesNotExist:
            item = None
        if item is not None:
            item.quantity = item.quantity + 1
            item.save()
        else:
            item = CartItems.objects.create(**validated_data)
        return item


class DeleteDeviceToken(serializers.Serializer):
    id = serializers.CharField(max_length=100)

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass


class AuthSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username')
