from django.urls import path
from api.views import *

app_name = "api"


urlpatterns = [
    path('sale_products/', SaleProductsViewSet.as_view(), name='sale_products'),
    path('new_products/', NewProductsViewSet.as_view(), name='new_products'),
    path('product/<int:pk>', ProductViewSet.as_view(), name='product'),
    path('categories/', CategoriesViewSet.as_view(), name='categories'),
    path('categories/<int:pk>', CategoriesProductViewSet.as_view(), name='categories_products'),
    path('order/create', CreateOrderViewSet.as_view(), name='create_order'),
    # path('order/<int:pk>', OrderViewSet.as_view(), name='order'),
    path('order/<int:id>/<int:pk>', OrderViewSet.as_view(), name='order'),
    # path('orders/', OrdersViewSet.as_view(), name='orders'),
    path('orders/<int:id>', OrdersViewSet.as_view(), name='orders'),
    path('reviews/<int:pk>', ReviewViewSet.as_view(), name='reviews'),
    # path('profile/me', ProfileViewSet.as_view(), name='profile'),
    path('profile/<int:id>', ProfileViewSet.as_view(), name='profile'),
    # path('cart/', CartViewSet.as_view(), name='cart'),
    path('cart/<int:id>', CartViewSet.as_view(), name='cart'),
    # path('cart/<int:pk>', CartItemViewSet.as_view(), name='cart_item'),
    path('cart/<int:id>/<int:pk>', CartItemViewSet.as_view(), name='cart_item'),
    path('cart/add', CartItemAddViewSet.as_view(), name='cart_item_add'),
    path('profile/create', ProfileCreateViewSet.as_view(), name='profile_create'),
    path('fcm/delete/<int:pk>', DeleteDeviceToken.as_view(), name='device_delete'),

    path('auth', AuthViewSet.as_view(), name='auth'),
]
