from django.contrib.auth import authenticate
from django.db.models import Sum, Q
from django.shortcuts import get_object_or_404
from djoser.serializers import User
from fcm.utils import get_device_model
from idna import unicode
from rest_framework import generics, permissions, status
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from api.serializers import ProductSerializer, CreateOrderSerializer, OrdersSerializer, \
    ReviewSerializer, ProfileSerializer, OrderSerializer, CategoriesSerializer, CartSerializer, ProfileCreateSerializer, \
    CartItemSerializer, CartItemAddSerializer, DeleteDeviceToken, AuthSerializer
from cart.models import Cart, CartItems
from orders.models import Order
from products.models import Product, Review, Category


class SaleProductsViewSet(generics.ListAPIView):
    serializer_class = ProductSerializer

    def get_queryset(self):
        size = int(self.request.GET.get('size'))
        if size == -1:
            queryset = Product.objects.filter(discount__gt=0, is_active=True)
        else:
            queryset = Product.objects.filter(discount__gt=0, is_active=True)[:size]
        return queryset


class NewProductsViewSet(generics.ListAPIView):
    serializer_class = ProductSerializer

    def get_queryset(self):
        size = int(self.request.GET.get('size'))
        if size == -1:
            queryset = Product.objects.filter(is_new=True, is_active=True)
        else:
            queryset = Product.objects.filter(is_new=True, is_active=True)[:size]
        return queryset


class ProductViewSet(generics.ListAPIView):
    serializer_class = ProductSerializer

    def get_queryset(self):
        queryset = Product.objects.filter(is_active=True, id=self.kwargs['pk'])
        return queryset


class CategoriesViewSet(generics.ListAPIView):
    queryset = Category.objects.filter(is_active=True)
    serializer_class = CategoriesSerializer


class CategoriesProductViewSet(generics.ListAPIView):
    serializer_class = ProductSerializer

    def get_queryset(self):
        if self.request.GET.get('sort') is None:
            sort = 'id'
        else:
            sort = self.request.GET.get('sort')

        if self.request.GET.get('type') is None or self.request.GET.get('type') == 'all':
            queryset = Product.objects.filter(is_active=True, category=self.kwargs['pk']).order_by(sort)
        elif self.request.GET.get('type') == 'new':
            queryset = Product.objects.filter(is_active=True, category=self.kwargs['pk'], is_new=True).order_by(sort)
        elif self.request.GET.get('type') == 'sale':
            queryset = Product.objects.filter(is_active=True, category=self.kwargs['pk'], discount__gt=0).order_by(sort)

        return queryset


class OrderViewSet(generics.ListAPIView):
    # permission_classes = (permissions.IsAuthenticated,)
    serializer_class = OrderSerializer

    def get_queryset(self):
        # user_id = self.request.user
        user_id = self.kwargs['id']
        queryset = Order.objects.filter(user_id=user_id, id=self.kwargs['pk'])
        return queryset


class OrdersViewSet(generics.ListAPIView):
    # permission_classes = (permissions.IsAuthenticated,)
    serializer_class = OrdersSerializer

    def get_queryset(self):
        # user_id = self.request.user
        user_id = self.kwargs['id']
        queryset = Order.objects.filter(user_id=user_id)
        return queryset


class CreateOrderViewSet(generics.CreateAPIView):
    serializer_class = CreateOrderSerializer

    def perform_create(self, serializer):
        if self.request.user.is_authenticated:
            # serializer.save(user=self.request.user)
            user = User.objects.get(id=self.kwargs['id'])
            serializer.save(user=user)
        else:
            serializer.save(user=None)


class ReviewViewSet(generics.ListCreateAPIView):
    serializer_class = ReviewSerializer

    def get_queryset(self):
        queryset = Review.objects.filter(product__id=self.kwargs['pk'], verificated=True)
        return queryset

    def perform_create(self, serializer):
        product = get_object_or_404(Product, id=self.kwargs['pk'])
        serializer.save(product=product)


class ProfileViewSet(generics.RetrieveUpdateAPIView):
    # permission_classes = (permissions.IsAuthenticated,)
    serializer_class = ProfileSerializer

    def retrieve(self, request, *args, **kwargs):
        # self.object = get_object_or_404(User, id=self.request.user.id)
        self.object = get_object_or_404(User, id=self.kwargs['id'])
        serializer = self.get_serializer(self.object)
        return Response(serializer.data)

    def get_queryset(self):
        # queryset = User.objects.filter(id=self.request.user.id)
        queryset = User.objects.filter(id=self.kwargs['id'])
        return queryset

    def get_object(self):
        return self.request.user


class ProfileCreateViewSet(generics.CreateAPIView):
    serializer_class = ProfileCreateSerializer
    queryset = 'ok'


class CartViewSet(generics.ListCreateAPIView):
    # permission_classes = (permissions.IsAuthenticated,)
    serializer_class = CartSerializer

    def get_queryset(self):
        user_id = self.request.user
        # queryset = Cart.objects.filter(user_id=user_id)
        queryset = Cart.objects.filter(user_id=self.kwargs['id'])
        return queryset


class CartItemViewSet(generics.RetrieveUpdateDestroyAPIView):
    # permission_classes = (permissions.IsAuthenticated,)
    serializer_class = CartItemSerializer

    def get_queryset(self):
        item_id = self.kwargs['pk']
        queryset = CartItems.objects.filter(id=item_id)
        return queryset


class CartItemAddViewSet(generics.CreateAPIView):
    # permission_classes = (permissions.IsAuthenticated,)
    serializer_class = CartItemAddSerializer


class DeleteDeviceToken(generics.DestroyAPIView):
    serializer_class = DeleteDeviceToken
    queryset = 'ok'

    def delete(self, request, *args, **kwargs):
        device = get_device_model()
        try:
            device.objects.get(dev_id=self.kwargs['pk']).delete()
            return Response({'delete': 'ok'}, status=status.HTTP_200_OK)
        except device.DoesNotExist:
            return Response({'delete': 'does not exist'}, status=status.HTTP_200_OK)

# WSGIPassAuthorization On


class AuthViewSet(APIView):
    serializer_class = AuthSerializer

    def post(self, request, format=None):
        username = request.data['username']
        password = request.data['password']

        user = authenticate(username=username, password=password)
        if user is not None:
            data = {
                'id': user.id,
                "first_name": '',
                "last_name": '',
                "email": '',
                "profile": {}
            }
            return Response(data, status=status.HTTP_200_OK)
        else:
            return Response({'id': 'none'}, status=status.HTTP_401_UNAUTHORIZED)
