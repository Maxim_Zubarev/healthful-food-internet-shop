from django.contrib import admin
from slider.models import Slider


class SliderAdmin(admin.ModelAdmin):
    list_display = ['name', 'is_active', 'created', 'updated']
    list_filter = ['is_active', 'created', 'updated']


admin.site.register(Slider, SliderAdmin)
