from django.db import models


def image_folder(instance, filename):
    filename = instance.name + '.' + filename.split('.')[1]
    return "slider/{1}".format(instance.name, filename)


class Slider(models.Model):
    name = models.CharField(max_length=100, verbose_name='Название', default='')
    title = models.CharField(max_length=255, verbose_name='Заголовок слайдера', default='')
    body = models.TextField(verbose_name='Текст слайдера')
    link = models.CharField(max_length=255, verbose_name='Ссылка', blank=True)
    image = models.ImageField(verbose_name='Изображение', upload_to=image_folder, default='')
    is_active = models.BooleanField(verbose_name='Показывать', default=True)
    created = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name='Дата создания')
    updated = models.DateTimeField(auto_now_add=False, auto_now=True, verbose_name='Дата обновления')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Слайдер'
        verbose_name_plural = 'Слайдеры'
